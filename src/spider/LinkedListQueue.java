package spider;

import java.util.*;

public class LinkedListQueue {
	// 字段
	private LinkedList<Object> list;

	// 无参数构造
	public LinkedListQueue() {
		list = new LinkedList<Object>();
	}

	// 队列元素的个数
	public int size() {
		return list.size();
	}

	// 进入队列
	public void enqueue(Object obj) {
		list.addLast(obj);
	}

	// 对头出列
	public Object dequeue() {
		return list.removeFirst();
	}

	// 浏览队头元素
	public Object front() {
		return list.peekFirst();
	}

	// 判断队头是否为空
	public boolean isEmpty() {
		return list.isEmpty();
	}
}
