package spider;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class FileRW {
	public static String getCode(URL url, String charSet) throws IOException {
		InputStream in = null;
		BufferedInputStream bin = null;
		InputStreamReader input = null;
		StringBuilder string = new StringBuilder();
		try {
			in = url.openConnection().getInputStream();
			bin = new BufferedInputStream(in);
			input = new InputStreamReader(bin, charSet);
			char[] b = new char[1024];
			int len;
			while ((len = input.read(b)) != -1) {
				string.append(new String(b, 0, len));
			}
		} catch (OutOfMemoryError ex) {
			throw new OutOfMemoryError("路径" + url.toString() + "内容获取失败");
		} catch (IOException ex) {
			throw new IOException("路径" + url.toString() + "内容获取错误");
		} finally {
			if (input != null)
				input.close();
			if (bin != null)
				bin.close();
			if (in != null)
				in.close();
		}
		return string.toString();
	}

	public static void write(String data) {
		try {
			File file = new File("result.txt");
			// 如果不存在该file则新建
			if (!file.exists()) {
				file.createNewFile();
			}
			// true = append file
			FileWriter fileWritter = new FileWriter(file.getName(), true);
			BufferedWriter bufferWritter = new BufferedWriter(fileWritter);
			bufferWritter.write(data + "\n");
			bufferWritter.close();

			// System.out.println("Done");

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
